import { Injectable } from '@nestjs/common';
import { PythonShell } from 'python-shell';
import {Subject} from "rxjs";
import {PythonRunnerService} from "../python-runner/python-runner.service";
@Injectable()
export class CalculationService {

    constructor(private readonly pythonRunnerService: PythonRunnerService) {
    }

  public async calculateGraphDegreeDistribution() {
      return await this.pythonRunnerService.runScript("degree_calculation.py");
  }
}
