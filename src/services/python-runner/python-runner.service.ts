import { Injectable } from '@nestjs/common';
import { PythonShell } from 'python-shell';
@Injectable()
export class PythonRunnerService {

    public async runScript(scriptName: string, parameters?: any) {
        const path = './src/scripts/'+scriptName;

        const { success, err = '', result } = await new Promise((resolve, reject) => {
            PythonShell.run(path, parameters, function (err, result) {
                if (err) {
                    reject({ success: false, err })
                }

                let resStr = "";

                result.forEach(i => {
                    resStr +=i;
                });

                resolve({ success: true, result: resStr });
                console.log('finished');
            });
        });

        if(err) {
            throw new Error("Python script execution error: "+ err);
        } else {
            return result;
        }
    }
}
