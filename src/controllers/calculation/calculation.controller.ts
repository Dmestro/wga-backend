import { Controller, Get } from '@nestjs/common';
import { CalculationService } from '../../services/calculation/calculation.service';

@Controller('calculation')
export class CalculationController {
  constructor(private readonly calculationService: CalculationService) {}

  @Get('graph-parameters')
  calculateGraphParameters() {
    console.log("TEST");
    return this.calculationService.calculateGraphDegreeDistribution();
  }
}
