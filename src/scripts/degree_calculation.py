#new actual results

import collections
import networkx as nx
import json
import math


def calculate_degree():

    G = nx.read_edgelist('./src/resources/cambridge_net.txt',
                         create_using=nx.DiGraph(),
                         nodetype=int)

    degree_sequence = sorted([d for n, d in G.degree()], reverse=True)
    degreeCount = collections.Counter(degree_sequence)
    deg, cnt = zip(*degreeCount.items())

    n = len(deg)

    x = deg
    y = cnt

    N = math.floor(1+ math.log(n)) + 1

    R = max(x) - min(x)

    delta = R/N

    u = [None] * (N+1)
    u[0] = min(x)

    for i in range(1, N+1):
        u[i] = u[i-1] + delta


    u_intervals = [None] * (len(u) - 1)

    for i in range(0, len(u) - 1):
        u_intervals[i] = [u[i], u[i+1]]

    n_i = [None] * N

    for i in range(0, len(u_intervals)):
        n_i[i] = 0
        for j in range(0, len(deg)):
            if(deg[j] >= u_intervals[i][0] and deg[j] <= u_intervals[i][1]):
                n_i[i] +=1

    n_mean_i = [None] * N

    for i in range(0, len(n_i)):
        n_mean_i[i] = n_i[i]/n

    h_i = [None] * len(n_i)

    for i in range(0, len(h_i)):
        h_i[i] = n_i[i]/ (delta * n)


    xx = u[1: len(u)]

    histogram = {"x": xx, "h": h_i, "columnWidth": delta}


    real_degree = {"degree": deg, "count": cnt}

    obj = {"realDegree": real_degree, "degreeDistribution": histogram}
    result = obj
    result = json.dumps(obj, sort_keys=True, indent=4)

    return result

res=calculate_degree()

print(res)