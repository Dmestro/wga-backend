import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CalculationController } from './controllers/calculation/calculation.controller';
import { CalculationService } from './services/calculation/calculation.service';
import {PythonRunnerService} from "./services/python-runner/python-runner.service";

@Module({
  imports: [],
  controllers: [AppController, CalculationController],
  providers: [AppService, CalculationService, PythonRunnerService],
})
export class AppModule {}
